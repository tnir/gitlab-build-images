## What does this MR do and why?

_Describe in detail what your merge request does and why._

<!--
Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

/label ~"type::maintenance" ~"maintenance::pipelines"

<!-- template sourced from https://gitlab.com/gitlab-org/gitlab-build-images/-/blob/master/.gitlab/merge_request_templates/Default.md -->
