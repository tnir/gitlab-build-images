# GitLab Build Images

This repository is used to build docker images that are used to build and test
various parts of GitLab:

1. Build Omnibus packages
1. Test GitLab-CE/EE project
1. Build gitlab-org/www-gitlab-com project

## Adding a new image

There are two methods to adding a new image:

1. Use a Dockerfile.
1. Use a custom image with versioned features.

### Use a Dockerfile

A bespoke image is one that uses it's own Dockerfile. In the interests of
avoiding repetition, it's better to use the custom docker build if possible.

Assuming the image you want to add is called `new-image`:

1. Add a test task: `new-image test: *test_build`.
1. Add a new build task: `new-image: *build_and_deploy`.
1. Add a Dockerfile: `Dockerfile.new-image`.

### Use a custom image with versioned features

To add a new image to the build, edit [`.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab-build-images/blob/master/.gitlab-ci.yml).

With custom images, the name of the image is used to define the contents (as opposed to use a bespoke Dockerfile build).

For this reason, the name needs to be correctly defined.

The name is defined as `[feature]-[version]` pairs, separated by `-`.

Here are some example build names:

1. `ruby-2.7-golang-1.15-git-2.29`
1. `ruby-3.0.0-git-2.29-lfs-2.9-node-14.15-yarn-1.22-graphicsmagick-1.3.34`
1. `ruby-3.0.0.patched-golang-1.14-git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-postgresql-12-graphicsmagick-1.3.34`

The first pair represents the base image and version. So `ruby-3.0-...` will use the `ruby:3.0` base image, while
`golang-1.15` will use the `golang:1.15` base image.

Each of the following parts represents another feature and version. Available
options are:

1. `bazelisk`
1. `chrome`
1. `docker`
1. `git`
1. `golang`
1. `graphicsmagick`
1. `lfs`
1. `node`
1. `postgresql`
1. `ruby`
1. `yarn`

#### Adding a new build

As an example, if you want to add new image for Ruby 3.0 with `git` 2.29 and `golang` 1.15,
the name would be `ruby-3.0-golang-1.15-git-2.29`.

1. Add a test task: `ruby-3.0-golang-1.15-git-2.29 test: *test_custom`
1. Add a new build task: `ruby-3.0-golang-1.15-git-2.29 push: *build_and_deploy_custom`

Note that the build name is also used for tagging the generated image, which means it cannot be
longer than 128 characters.

#### Pushing a rebuild image

To build (or rebuild) a given image and push it, you need to enable manual action for a given
job after it is merged to master.

By default we don't do it to ensure that an upstream dependency doesn't end up breaking our images
unexpectedly. For reference, this happened in the past: https://gitlab.com/gitlab-org/gitlab/issues/205192

## Note regarding Google Chrome

Google has a policy of yanking older versions of Google Chrome from their PPA
whenever a new major version is released. To help maintain consistent build
images, there is a CI step that saves the latest Google Chrome .deb into an the
GitLab package registry. The install for Chrome will attempt to retrieve from the
registry if it cannot find the file in the apt repository.
See `scripts/cache-google-chrome` for more details.

## Contributing

See [Contributing](CONTRIBUTING.md)
